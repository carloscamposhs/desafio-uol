package com.carlos.core;

import org.junit.runner.RunWith;

import io.cucumber.junit.Cucumber;
import io.cucumber.junit.CucumberOptions;

/**
 * Classe responsável por configurar o cucumber
 * Definindo o local dos steps, features e saída de relatório
 */
@RunWith(Cucumber.class)
@CucumberOptions(
		glue = "com.carlos.steps",
		dryRun = false,
		monochrome = true,
		features = "src/test/resources/features",
		plugin = {"json:report/json/cucumber.json"})
public class CucumberConfig {

}