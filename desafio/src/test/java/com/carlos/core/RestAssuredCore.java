package com.carlos.core;

import io.restassured.RestAssured;
import io.restassured.http.ContentType;
import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;

public class RestAssuredCore {
	public static String BASE_URI = null;
	private static RequestSpecification request;
	protected static Response response;
	
	public static void start() {
		 request = RestAssured.with().contentType(ContentType.JSON).with().baseUri(BASE_URI);
	}

	public static RequestSpecification getRequestSpecification() {
		return request;
	}
}