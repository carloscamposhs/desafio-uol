package com.carlos.core;

public class StepException extends Exception{

	private static final long serialVersionUID = 3884757496431115195L;
	
	public StepException(String message) {
		super(message);
	}
}