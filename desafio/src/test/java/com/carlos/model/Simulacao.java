package com.carlos.model;

public class Simulacao {
	private Integer id;
	private String cpf;
	private String nome;
	private String email;
	private Integer valor;
	private Integer parcelas;
	private Boolean seguro;
	
	public String getCpf() {
		return cpf;
	}
	public void setCpf(String cpf) {
		this.cpf = cpf;
	}
	public String getNome() {
		return nome;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public Integer getValor() {
		return valor;
	}
	public void setValor(Integer valor) {
		this.valor = valor;
	}
	public Integer getParcelas() {
		return parcelas;
	}
	public void setParcelas(Integer parcela) {
		this.parcelas = parcela;
	}
	public Boolean isSeguro() {
		return seguro;
	}
	public void setSeguro(Boolean seguro) {
		this.seguro = seguro;
	}
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	
	public static Simulacao gerarAtualizacaoDeSimulacaoValida() {
		Simulacao simulacao = new Simulacao();
		simulacao.setEmail("email_novo@email_novo.com");
		simulacao.setNome("Carlos Henrique Campos");
		simulacao.setParcelas(24);
		simulacao.setValor(4000);
		simulacao.setSeguro(false);
		return simulacao;
	}
	
	public static Simulacao gerarSimulacaoValida() {
		Simulacao simulacao = new Simulacao();
		simulacao.setEmail("email@email.com");
		simulacao.setNome("Carlos Henrique");
		simulacao.setParcelas(12);
		simulacao.setValor(2000);
		simulacao.setSeguro(true);
		return simulacao;
	}
}