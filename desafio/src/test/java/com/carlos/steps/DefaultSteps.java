package com.carlos.steps;

import com.carlos.core.RestAssuredCore;
import com.carlos.core.StepException;

import io.cucumber.java.Before;
import io.cucumber.java.en.Then;

/**
 * Classe com os STEPS padrões e hooks de execução
 */
public class DefaultSteps extends RestAssuredCore{
	private static boolean beforeAll = true;
	
	@Before
	public void beforeAll() {
		if(beforeAll) {
			RestAssuredCore.BASE_URI = System.getProperty("host", "http://localhost:8080/");
			beforeAll = false;
		}
	}
	
	@Before
	public void TestSetup() {
		RestAssuredCore.start();
	}
	
	
	@Then("^O código de resposta deve ser \"(\\d+)\"")
	public void o_codigo_de_resposta_deve_ser(String cod) throws StepException {
		if(response == null) throw new StepException("Não houve resposta da API.");
		response.then().assertThat().statusCode(Integer.parseInt(cod));
	}
}