package com.carlos.steps;

import static org.hamcrest.Matchers.equalTo;

import com.carlos.core.RestAssuredCore;
import com.carlos.core.StepException;

import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import io.restassured.specification.RequestSpecification;

/**
 * Classe com os STEPS Cucumber para a api restrições
 */
public class RestricoesSteps extends RestAssuredCore{
	private RequestSpecification request = RestAssuredCore.getRequestSpecification();
	
	@Given("^Que a API restrições está desenvolvida e operando$")
	public void que_a_API_restricoes_esta_desenvolvida_e_operando() {
		request = RestAssuredCore.getRequestSpecification();
	}
	
	@When("^Valido se o CPF \"(.*?)\" possui restrições$")
	public void valido_se_o_cpf_possui_restricoes(String cpf) {
		response = request.when().get("api/v1/restricoes/"+cpf);
	}
	
	@Then("^Mensagem \"(.*?)\" deve ser exibida ao consultar a restrição")
	public void mensagem_deve_ser_exibida_ao_consultar_a_restricao(String mensagem) throws StepException {
		if(response == null) throw new StepException("Não houve resposta da API.");
		response.then().assertThat().body("mensagem", equalTo(mensagem));
	}
}