package com.carlos.steps;

import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.hasKey;

import com.carlos.core.RestAssuredCore;
import com.carlos.core.StepException;
import com.carlos.model.Simulacao;

import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import io.restassured.specification.RequestSpecification;

/**
 * Classe com os STEPS Cucumber para a api simulação
 */
public class SimulacaoSteps extends RestAssuredCore{
	private RequestSpecification request = RestAssuredCore.getRequestSpecification();
	
	@Given("^Que a API simulações está desenvolvida e operando$")
	public void que_a_API_simulacoes_esta_desenvolvidada_e_operando() {
		request = RestAssuredCore.getRequestSpecification();
	}
	
	@When("^Consulto a lista de simulações$")
	public void consulto_a_lista_de_simulacoes() {
		response = request.given().body(new Simulacao()).when().get("api/v1/simulacoes");
	}
	
	@When("^Faço uma chamada de criação de simulação sem informar os campos obrigatórios$")
	public void faco_uma_chamada_de_criacao_de_simulacao_sem_informar_os_campos_obrigatorios() {
		response = request.given().body(new Simulacao()).when().post("api/v1/simulacoes");
	}

	@When("^Faço uma chamada de criação de simulação informando um E-mail inválido$")
	public void faco_uma_chamada_de_criacao_de_simulacao_informando_um_email_invalido() {
		Simulacao simulacao = new Simulacao();
		simulacao.setEmail("email_invalido");
		response = request.given().body(simulacao).when().post("api/v1/simulacoes");
	}
	
	@When("^Faço uma chamada de criação de simulação informando o valor \"(.*?)\"$")
	public void faco_uma_chamada_de_criacao_de_simulacao_informando_o_valor(String valor) {
		Integer valorFinal;
		try {
			valorFinal = Integer.parseInt(valor);
		} catch (NumberFormatException e) {
			throw new NumberFormatException("Valor informado inválido.");
		}
		Simulacao simulacao = new Simulacao();
		simulacao.setValor(valorFinal);
		response = request.given().body(simulacao).when().post("api/v1/simulacoes");
	}
	
	@When("^Faço uma chamada de criação de simulação informando a quantidade de parcelas \"(\\d+?)\"$")
	public void faco_uma_chamada_de_criacao_de_simulacao_informando_a_quantidade_de_parcelas(String parcelas) {
		int parcelasFinal = Integer.parseInt(parcelas);
		Simulacao simulacao = new Simulacao();
		simulacao.setParcelas(parcelasFinal);
		response = request.given().body(simulacao).when().post("api/v1/simulacoes");
	}
	
	@When("^Faço uma chamada válida de criação de simulação com o CPF \"(\\d+?)\"$")
	public void faco_uma_chamada_valida_de_criacao_de_simulacao_com_o_cpf(String cpf) {
		Simulacao simulacao = Simulacao.gerarSimulacaoValida();
		simulacao.setCpf(cpf);
		response = request.given().body(simulacao).when().post("api/v1/simulacoes");
	}
	
	@When("^Faço uma chamada de alteração de simulação no CPF \"(\\d+?)\" sem informar os campos obrigatórios$")
	public void faco_uma_chamada_de_alteracao_de_simulacao_no_cpf_sem_informar_os_campos_obrigatorios(String cpf) {
		Simulacao simulacao = new Simulacao();
		simulacao.setCpf(cpf);
		response = request.given().body(simulacao).when().put("api/v1/simulacoes/"+cpf);
	}
	
	@When("^Faço uma chamada de alteração de simulação no CPF \"(\\d+?)\" informando um E-mail inválido$")
	public void faco_uma_chamada_de_alteracao_de_simulacao_no_cpf_informando_um_email_invalido(String cpf) {
		Simulacao simulacao = new Simulacao();
		simulacao.setEmail("email_invalido");
		response = request.given().body(simulacao).when().put("api/v1/simulacoes/"+cpf);
	}
	
	@When("^Faço uma chamada de alteração de simulação no CPF \"(\\d+?)\" informando o valor \"(.*?)\"$")
	public void faco_uma_chamada_de_alteracao_de_simulacao_no_cpf_informando_o_valor(String cpf, String valor) {
		Integer valorFinal;
		try {
			valorFinal = Integer.parseInt(valor);
		} catch (NumberFormatException e) {
			throw new NumberFormatException("Valor informado inválido.");
		}
		Simulacao simulacao = new Simulacao();
		simulacao.setValor(valorFinal);
		response = request.given().body(simulacao).when().put("api/v1/simulacoes/"+cpf);
	}
	
	@When("^Faço uma chamada de alteração de simulação no CPF \"(\\d+?)\" informando a quantidade de parcelas \"(\\d+?)\"$")
	public void faco_uma_chamada_de_alteracao_de_simulacao_no_cpf_informando_a_quantidade_de_parcelas(String cpf, String parcelas) {
		int parcelasFinal = Integer.parseInt(parcelas);
		Simulacao simulacao = new Simulacao();
		simulacao.setParcelas(parcelasFinal);
		response = request.given().body(simulacao).when().put("api/v1/simulacoes/"+cpf);
	}
	
	@When("^Faço uma chamada de alteração de simulação no CPF \"(\\d+?)\" alterando o CPF para \"(\\d+?)\"$")
	public void faco_uma_chamada_de_alteracao_de_simulacao_no_cpf_alterando_o_cpf_para(String cpf, String novoCpf) {
		Simulacao simulacao = Simulacao.gerarAtualizacaoDeSimulacaoValida();
		simulacao.setCpf(novoCpf);
		response = request.given().body(simulacao).when().put("api/v1/simulacoes/"+cpf);
	}
	
	@When("^Faço a consulta da simulação do CPF \"(\\d+?)\"$")
	public void faco_uma_consulta_da_simulacao_do_cpf(String cpf) {
		response = request.given().when().get("api/v1/simulacoes/"+cpf);
	}
	
	@When("^Faço a removação da simulação do CPF \"(\\d+?)\"$")
	public void faco_a_remocao_da_simulacao_do_CPF(String cpf) {
		response = request.given().when().delete("api/v1/simulacoes/"+cpf);
	}
	
	@Then("^Simulação é criada com sucesso para o CPF \"(\\d+?)\"$")
	public void simulacao_e_criada_com_sucesso_para_o_cpf(String cpf) {
		Simulacao simulacao = Simulacao.gerarSimulacaoValida();
		simulacao.setCpf(cpf);
		response.then().assertThat().body("nome", equalTo(simulacao.getNome())).and().
						assertThat().body("cpf", equalTo(simulacao.getCpf())).and().
						assertThat().body("email", equalTo(simulacao.getEmail())).and().
						assertThat().body("parcelas", equalTo(simulacao.getParcelas())).and().
						assertThat().body("seguro", equalTo(simulacao.isSeguro())).and().
						assertThat().body("valor", equalTo(simulacao.getValor()));
	}
	
	@Then("^Simulação é atualizada com sucesso para com o novo CPF \"(\\d+?)\"$")
	public void simulacao_e_atualizada_com_sucesso_para_com_o_novo_cpf(String cpf) {
		Simulacao simulacao = Simulacao.gerarAtualizacaoDeSimulacaoValida();
		simulacao.setCpf(cpf);
		response.then().assertThat().body("nome", equalTo(simulacao.getNome())).and().
						assertThat().body("cpf", equalTo(simulacao.getCpf())).and().
						assertThat().body("email", equalTo(simulacao.getEmail())).and().
						assertThat().body("parcelas", equalTo(simulacao.getParcelas())).and().
						assertThat().body("seguro", equalTo(simulacao.isSeguro())).and().
						assertThat().body("valor", equalTo(simulacao.getValor()));
	}
	
	@Then("^Mensagem de erro \"(.*?)\" deve ser exibida para o campo \"(.*?)\"$")
	public void mensagem_de_erro_deve_ser_exibida_para_o_campo(String mensagem, String campo) throws StepException {
		if(response == null) throw new StepException("Não houve resposta da API.");
		response.then().assertThat().body("erros", hasKey(campo.toLowerCase()));
		response.then().assertThat().body("erros."+campo.toLowerCase(), equalTo(mensagem));
	}
	
	@Then("^Mensagem \"(.*?)\" deve ser exibida ao criar a simulação")
	public void mensagem_deve_ser_exibida_ao_criar_a_simulacao(String mensagem) throws StepException {
		if(response == null) throw new StepException("Não houve resposta da API.");
		response.then().assertThat().body("mensagem", equalTo(mensagem));
	}
	
	@Then("^Mensagem \"(.*?)\" deve ser exibida ao alterar a simulação")
	public void mensagem_deve_ser_exibida_ao_alterar_a_simulacao(String mensagem) throws StepException {
		if(response == null) throw new StepException("Não houve resposta da API.");
		response.then().assertThat().body("mensagem", equalTo(mensagem));
	}
	
	@Then("^A lista de simulações deve ser exibida")
	public void a_lista_de_simulacoes_deve_ser_exibida() throws StepException {
		if(response == null) throw new StepException("Não houve resposta da API.");
		response.then().extract().as(Simulacao[].class);
	}
	
	@Then("^A simulação deve ser exibida")
	public void a_simulacao_deve_ser_exibida() throws StepException {
		if(response == null) throw new StepException("Não houve resposta da API.");
		response.then().extract().as(Simulacao.class);
	}
}