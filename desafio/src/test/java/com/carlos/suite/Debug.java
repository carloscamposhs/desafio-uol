package com.carlos.suite;

import com.carlos.core.CucumberConfig;

import io.cucumber.junit.CucumberOptions;

/**
 * Suíte responsável por executar os cenários especificos durante o desenvolvimento
 * Para isso adicione a tag @Debug nos cenários desejados
 */
@CucumberOptions(tags = "@Debug")
public class Debug extends CucumberConfig{

}