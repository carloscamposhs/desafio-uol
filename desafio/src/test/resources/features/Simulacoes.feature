Feature: Validar a API de 'simulacoes'
		
	Scenario: CT0201 Validar que os campos são obrigatórios ao criar simulação
		Given Que a API simulações está desenvolvida e operando
		When Faço uma chamada de criação de simulação sem informar os campos obrigatórios
		Then O código de resposta deve ser "400"
		And Mensagem de erro "Parcelas não pode ser vazio" deve ser exibida para o campo "Parcelas"
		And Mensagem de erro "Valor não pode ser vazio" deve ser exibida para o campo "Valor"
		And Mensagem de erro "CPF não pode ser vazio" deve ser exibida para o campo "CPF"
		And Mensagem de erro "Nome não pode ser vazio" deve ser exibida para o campo "Nome"
		And Mensagem de erro "E-mail não deve ser vazio" deve ser exibida para o campo "Email"
		
	Scenario: CT0202 Validar erro ao informar e-mail inválido ao cadastrar simulação
		Given Que a API simulações está desenvolvida e operando
		When Faço uma chamada de criação de simulação informando um E-mail inválido
		Then O código de resposta deve ser "400"
		And Mensagem de erro "E-mail deve ser um e-mail válido" deve ser exibida para o campo "Email"
		
	Scenario: CT0203 Validar erro ao informar um valor inferior a 1000 ao cadastrar simulação
		Given Que a API simulações está desenvolvida e operando
		When Faço uma chamada de criação de simulação informando o valor "999"
		Then O código de resposta deve ser "400"
		And Mensagem de erro "Valor deve ser maior ou igual a R$ 1.000" deve ser exibida para o campo "Valor"
		
	Scenario: CT0204 Validar erro ao informar um valor superior a 40000 ao cadastrar simulação
		Given Que a API simulações está desenvolvida e operando
		When Faço uma chamada de criação de simulação informando o valor "40001"
		Then O código de resposta deve ser "400"
		And Mensagem de erro "Valor deve ser menor ou igual a R$ 40.000" deve ser exibida para o campo "Valor"
		
	Scenario: CT0205 Validar erro ao informar parcelas menor que 2 ao cadastrar simulação
		Given Que a API simulações está desenvolvida e operando
		When Faço uma chamada de criação de simulação informando a quantidade de parcelas "1"
		Then O código de resposta deve ser "400"
		And Mensagem de erro "Parcelas deve ser igual ou maior que 2" deve ser exibida para o campo "Parcelas"
				
	Scenario: CT0206 Validar erro ao informar parcelas maior que 48 ao cadastrar simulação
		Given Que a API simulações está desenvolvida e operando
		When Faço uma chamada de criação de simulação informando a quantidade de parcelas "49"
		Then O código de resposta deve ser "400"
		And Mensagem de erro "Parcelas deve ser igual ou mmenor que 48" deve ser exibida para o campo "Parcelas"	
	
	Scenario: CT0207 Validar que a criação com sucesso de simulação com dados válidos
		Given Que a API simulações está desenvolvida e operando
		When Faço uma chamada válida de criação de simulação com o CPF "01234567890"
		Then O código de resposta deve ser "201"
		And Simulação é criada com sucesso para o CPF "01234567890"
		
	Scenario: CT0208 Validar erro ao cadastrar uma simulação com CPF já existente 
		Given Que a API simulações está desenvolvida e operando
		When Faço uma chamada válida de criação de simulação com o CPF "01234567890"
		Then O código de resposta deve ser "409"
		And Mensagem "CPF duplicado" deve ser exibida ao criar a simulação
	
	Scenario: CT0209 Validar erro ao informar e-mail inválido ao atualizar simulação
		Given Que a API simulações está desenvolvida e operando
		When Faço uma chamada de alteração de simulação no CPF "01234567890" informando um E-mail inválido
		Then O código de resposta deve ser "400"
		And Mensagem de erro "E-mail deve ser um e-mail válido" deve ser exibida para o campo "Email"

	Scenario: CT0210 Validar erro ao informar um valor inferior a 1000 ao alterar simulação
		Given Que a API simulações está desenvolvida e operando
		When Faço uma chamada de alteração de simulação no CPF "01234567890" informando o valor "999"
		Then O código de resposta deve ser "400"
		And Mensagem de erro "Valor deve ser maior ou igual a R$ 1.000" deve ser exibida para o campo "Valor"
				
	Scenario: CT0211 Validar erro ao informar um valor superior a 40000 ao alterar simulação
		Given Que a API simulações está desenvolvida e operando
		When Faço uma chamada de alteração de simulação no CPF "01234567890" informando o valor "40001"
		Then O código de resposta deve ser "400"
		And Mensagem de erro "Valor deve ser maior ou igual a R$ 1.000" deve ser exibida para o campo "Valor"
		
	Scenario: CT0212 Validar erro ao informar parcelas inferior a 2 ao atualizar simulação
		Given Que a API simulações está desenvolvida e operando
		When Faço uma chamada de alteração de simulação no CPF "01234567890" informando a quantidade de parcelas "1"
		Then O código de resposta deve ser "400"
		And Mensagem de erro "Parcelas deve ser igual ou maior que 2" deve ser exibida para o campo "Parcelas"
	
	Scenario: CT0213 Validar erro ao informar parcelas superior a 48 ao atualizar simulação
		Given Que a API simulações está desenvolvida e operando
		When Faço uma chamada de alteração de simulação no CPF "01234567890" informando a quantidade de parcelas "49"
		Then O código de resposta deve ser "400"
		And Mensagem de erro "Parcelas deve ser igual ou maior que 2" deve ser exibida para o campo "Parcelas"
		
	Scenario: CT0214 Validar erro ao informar CPF inexistente ao atualizar simulação
		Given Que a API simulações está desenvolvida e operando
		When Faço uma chamada de alteração de simulação no CPF "0000" alterando o CPF para "1111"
		Then O código de resposta deve ser "404"
		And Mensagem "CPF não encontrado" deve ser exibida ao alterar a simulação
		
	Scenario: CT0215 Validar a atualização válida de uma simulação
		Given Que a API simulações está desenvolvida e operando
		When Faço uma chamada de alteração de simulação no CPF "01234567890" alterando o CPF para "01234567891"
		Then O código de resposta deve ser "200"
		And Simulação é atualizada com sucesso para com o novo CPF "01234567891"
	
	Scenario: CT0216 Validar erro ao fazer atualização de uma simulação alterando o CPF para um já em uso
		Given Que a API simulações está desenvolvida e operando
		When Faço uma chamada de alteração de simulação no CPF "01234567891" alterando o CPF para "17822386034"
		Then O código de resposta deve ser "409"
		And Mensagem "CPF já existente" deve ser exibida ao alterar a simulação

	Scenario: CT0217 Validar consulta de simulações
		Given Que a API simulações está desenvolvida e operando
		When Consulto a lista de simulações
		Then O código de resposta deve ser "200"
		And A lista de simulações deve ser exibida
	
	Scenario: CT0218 Validar consulta de simulação pelo CPF
		Given Que a API simulações está desenvolvida e operando
		When Faço a consulta da simulação do CPF "17822386034"
		Then O código de resposta deve ser "200"
		And A simulação deve ser exibida

	Scenario: CT0219 Validar erro ao consultar simulação de um CPF inexistente
		Given Que a API simulações está desenvolvida e operando
		When Faço a consulta da simulação do CPF "1111" 
		Then O código de resposta deve ser "404"

	Scenario: CT0220 Validar remoção válida de simulação
		Given Que a API simulações está desenvolvida e operando
		When Faço a removação da simulação do CPF "01234567891"
		Then O código de resposta deve ser "204"
	
	@Debug
	Scenario: CT0221 Validar erro ao remover simulação de CPF inexistente
		Given Que a API simulações está desenvolvida e operando
		When Faço a removação da simulação do CPF "01234567891"
		Then O código de resposta deve ser "404"
		And Mensagem "Simulação não encontrada" deve ser exibida ao alterar a simulação			