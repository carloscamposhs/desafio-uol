Feature: Validar a API 'restrições'

	Scenario: CT0101 Validar reposta da API para CPF com restrição
		Given Que a API restrições está desenvolvida e operando
		When Valido se o CPF "97093236014" possui restrições
		Then O código de resposta deve ser "200"
		And Mensagem "O CPF 97093236014 possui restrição" deve ser exibida ao consultar a restrição
		
	Scenario: CT0102 Validar reposta da API para CPF sem restrição
		Given Que a API restrições está desenvolvida e operando
		When Valido se o CPF "01234567890" possui restrições
		Then O código de resposta deve ser "204"